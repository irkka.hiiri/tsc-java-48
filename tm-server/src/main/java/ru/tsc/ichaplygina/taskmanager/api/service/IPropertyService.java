package ru.tsc.ichaplygina.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.property.*;

public interface IPropertyService extends IPasswordProperty, IApplicationProperty, IAutosaveProperty,
        IFileScannerProperty, ISessionProperty, INetworkProperty, IDatabaseProperty {

}
