package ru.tsc.ichaplygina.taskmanager.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.ILogService;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static java.util.logging.Level.WARNING;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@NoArgsConstructor
public final class LogService implements ILogService {

    @NotNull
    private static final String COMMANDS = "COMMANDS";

    @NotNull
    private static final String COMMAND_FILE = "access.log";

    @NotNull
    private static final Logger COMMAND_LOGGER = Logger.getLogger(COMMANDS);

    @NotNull
    private static final String ERRORS = "ERRORS";

    @NotNull
    private static final String ERROR_FILE = "errors.log";

    @NotNull
    private static final Logger ERROR_LOGGER = Logger.getLogger(ERRORS);

    @NotNull
    private static final String LOGGER_PROPERTIES_FILE = "logging.properties";

    @NotNull
    private static final String MESSAGES = "MESSAGES";

    @NotNull
    private static final String MESSAGES_FILE = "messages.log";

    @NotNull
    private static final Logger MESSAGE_LOGGER = Logger.getLogger(MESSAGES);

    @NotNull
    private static final Logger ROOT_LOGGER = Logger.getGlobal();

    @NotNull
    private final LogManager logManager = LogManager.getLogManager();

    private boolean consoleEnabled = true;

    {
        initProperties();
        registerHandlers(COMMAND_LOGGER, COMMAND_FILE);
        registerHandlers(ERROR_LOGGER, ERROR_FILE);
        registerHandlers(MESSAGE_LOGGER, MESSAGES_FILE);
        if (consoleEnabled) {
            COMMAND_LOGGER.addHandler(new ConsoleHandler());
            ERROR_LOGGER.addHandler(new ConsoleHandler());
        }
    }

    public LogService(final boolean consoleEnabled) {
        this.consoleEnabled = consoleEnabled;
    }

    @Override
    public final void command(@Nullable final String message) {
        if (isEmptyString(message)) return;
        COMMAND_LOGGER.info(message);
    }

    @Override
    public final void error(final @NotNull Exception e) {
        ERROR_LOGGER.log(WARNING, e.getMessage(), e);
    }

    public final boolean getConsoleEnabled() {
        return consoleEnabled;
    }

    public final void setConsoleEnabled(boolean consoleEnabled) {
        this.consoleEnabled = consoleEnabled;
    }

    @Override
    public final void info(@Nullable final String message) {
        if (isEmptyString(message)) return;
        MESSAGE_LOGGER.info(message);
    }

    public final void initProperties() {
        try {
            @Nullable final InputStream configFile = ClassLoader.getSystemResourceAsStream(LOGGER_PROPERTIES_FILE);
            if (configFile == null) {
                ROOT_LOGGER.log(SEVERE, "Error! Cannot find file: " + LOGGER_PROPERTIES_FILE);
                return;
            }
            logManager.readConfiguration(configFile);
        } catch (@NotNull final IOException e) {
            ROOT_LOGGER.log(SEVERE, "Error! Failed to initialize logging properties: " + e.getMessage(), e);
        }
    }

    public final void registerHandlers(@NotNull final Logger logger, @NotNull final String fileName) {
        try {
            logger.addHandler(new FileHandler(fileName));
        } catch (@NotNull final IOException e) {
            ROOT_LOGGER.severe("Error! Failed to register logging file handlers: " + e.getMessage());
        }
    }

}
