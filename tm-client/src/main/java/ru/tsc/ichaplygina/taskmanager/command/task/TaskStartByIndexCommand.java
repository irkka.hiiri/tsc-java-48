package ru.tsc.ichaplygina.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "start task by index";

    @NotNull
    public final static String DESCRIPTION = "start task by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int index = readNumber(INDEX_INPUT);
        getTaskEndpoint().startTaskByIndex(getSession(), index);
    }

}
