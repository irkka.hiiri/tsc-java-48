package ru.tsc.ichaplygina.taskmanager.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public class DomainLoadJsonJaxbCommand extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "load json jaxb";

    @NotNull
    public final static String DESCRIPTION = "load projects, tasks and users from json file using jaxb";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }


    @Override
    @SneakyThrows
    public void execute() {
        getAdminEndpoint().loadJsonJaxb(getSession());
    }

}
